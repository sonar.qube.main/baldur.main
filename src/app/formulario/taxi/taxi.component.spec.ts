import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { TaxiComponent } from './taxi.component';
import { HeaderTipoAComponent } from 'src/app/componente/global/header-tipo-a/header-tipo-a.component';
import { BotonTipoAComponent } from 'src/app/componente/global/boton-tipo-a/boton-tipo-a.component';

describe('TaxiComponent', () => {
  let component: TaxiComponent;
  let fixture: ComponentFixture<TaxiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TaxiComponent,
        HeaderTipoAComponent,
        BotonTipoAComponent,
      ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TaxiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid initially', () => {
    expect(component.taxiForm.valid).toBeFalsy();
  });

  it('should be valid after filling the form correctly', () => {
    component.taxiForm.controls['titulo'].setValue('Test Taxi');
    component.taxiForm.controls['detalle'].setValue('Detalle del taxi');
    component.taxiForm.controls['origen'].setValue('Inicio');
    component.taxiForm.controls['destino'].setValue('Final');
    component.taxiForm.controls['distanciaKM'].setValue(20);
    component.taxiForm.controls['costoBS'].setValue(50);
    expect(component.taxiForm.valid).toBeTruthy();
  });

  it('should show error message when trying to submit invalid form', () => {
    component.onSubmit();
    expect(component.errorMessage).toBe('El formulario no es válido. Por favor, corrija los errores e intente nuevamente.');
  });
});
