import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeriendaComponent } from './formulario/merienda/merienda.component';
import { TaxiComponent } from './formulario/taxi/taxi.component';

const routes: Routes = [
  {path: 'formularioMerienda', component: MeriendaComponent},
  {path: 'formularioTaxi', component: TaxiComponent},
  { path: '', redirectTo: '/merienda', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
