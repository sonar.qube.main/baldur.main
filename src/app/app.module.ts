import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BotonTipoAComponent } from './componente/global/boton-tipo-a/boton-tipo-a.component';
import { BotonTipoBComponent } from './componente/global/boton-tipo-b/boton-tipo-b.component';
import { HeaderTipoAComponent } from './componente/global/header-tipo-a/header-tipo-a.component';
import { HeaderTipoBComponent } from './componente/global/header-tipo-b/header-tipo-b.component';
import { TaxiComponent } from './formulario/taxi/taxi.component';
import { MeriendaComponent } from './formulario/merienda/merienda.component';
import { NavbarComponent } from './main/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { BodyComponent } from './main/body/body.component';
import { FooterComponent } from './main/footer/footer.component';
import { SkelComponent } from './main/skel/skel.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';


@NgModule({
  declarations: [
    AppComponent,
    BotonTipoAComponent,
    BotonTipoBComponent,
    HeaderTipoAComponent,
    HeaderTipoBComponent,
    TaxiComponent,
    MeriendaComponent,
    NavbarComponent,
    BodyComponent,
    FooterComponent,
    SkelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatMenuModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDividerModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
