export interface IMerienda {
    titulo: string;
    detalle: string;
    cantidad: number;
    precioUnitarioBS: number;
    totalBS: number;
}
