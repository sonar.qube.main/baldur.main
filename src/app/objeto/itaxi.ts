export interface ITaxi {
    titulo: string;
    detalle: string;
    origen: string;
    destino: string;
    distanciaKM: number;
    costoBS: number;
}
