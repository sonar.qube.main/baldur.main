import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonTipoBComponent } from './boton-tipo-b.component';

describe('BotonTipoBComponent', () => {
  let component: BotonTipoBComponent;
  let fixture: ComponentFixture<BotonTipoBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BotonTipoBComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BotonTipoBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
