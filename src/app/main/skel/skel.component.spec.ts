import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkelComponent } from './skel.component';
import { NavbarComponent } from '../navbar/navbar.component';
import { BodyComponent } from '../body/body.component';
import { FooterComponent } from '../footer/footer.component';
import { MatToolbar } from '@angular/material/toolbar';
import { MatMenu } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';

describe('SkelComponent', () => {
  let component: SkelComponent;
  let fixture: ComponentFixture<SkelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        SkelComponent,
        NavbarComponent,
        BodyComponent,
        FooterComponent,
        MatToolbar,
        MatMenu
      ],
      imports: [
        RouterTestingModule
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SkelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
