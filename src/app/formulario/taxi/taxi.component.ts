import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ITaxi } from 'src/app/objeto/itaxi';
import { jsPDF } from 'jspdf';

@Component({
  selector: 'app-taxi',
  templateUrl: './taxi.component.html',
  styleUrls: ['./taxi.component.css']
})
export class TaxiComponent implements OnInit {

  public taxiForm!: FormGroup;
  public errorMessage: string | null = null;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.taxiForm = this.fb.group({
      titulo: ['', Validators.required],
      detalle: ['', Validators.required],
      origen: ['', Validators.required],
      destino: ['', Validators.required],
      distanciaKM: [null, [Validators.required, Validators.min(1)]],
      costoBS: [null, [Validators.required, Validators.min(1)]]
    });
  }

  onSubmit() {
    if (this.taxiForm.valid) {
      this.errorMessage = null; // Resetear el mensaje de error
      const taxiData: ITaxi = this.taxiForm.value;
      this.generatePDF(taxiData);
    } else {
      this.errorMessage = 'El formulario no es válido. Por favor, corrija los errores e intente nuevamente.';
    }
  }

  private generatePDF(taxiData: ITaxi) {
    const doc = new jsPDF();
    doc.setFontSize(22);
    doc.text('Datos del Taxi', 10, 10);
    doc.setFontSize(12);
    doc.text('Título: ' + taxiData.titulo, 10, 30);
    doc.text('Detalle: ' + taxiData.detalle, 10, 40);
    doc.text('Origen: ' + taxiData.origen, 10, 50);
    doc.text('Destino: ' + taxiData.destino, 10, 60);
    doc.text('Distancia en KM: ' + taxiData.distanciaKM, 10, 70);
    doc.text('Costo en BS: ' + taxiData.costoBS, 10, 80);

    doc.save('Taxi.pdf');
  }
}
