import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonTipoAComponent } from './boton-tipo-a.component';

describe('BotonTipoAComponent', () => {
  let component: BotonTipoAComponent;
  let fixture: ComponentFixture<BotonTipoAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BotonTipoAComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BotonTipoAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should activate', () => {
    spyOn(component.btnClicked, 'emit');
    component.onButtonClick();
    expect(component.btnClicked.emit).toHaveBeenCalled();
  });

  it('should print to console on button click test', () => {
    spyOn(console, 'log');
    component.onButtonClickTest();
    expect(console.log).toHaveBeenCalledWith('BotonTipoAComponent');
  });
});
