import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { jsPDF } from 'jspdf';

@Component({
  selector: 'app-merienda',
  templateUrl: './merienda.component.html',
  styleUrls: ['./merienda.component.css']
})
export class MeriendaComponent implements OnInit {

  public meriendaForm!: FormGroup;
  public errorMessage: string | null = null;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.meriendaForm = this.fb.group({
      titulo: ['', Validators.required],
      detalle: ['', Validators.required],
      cantidad: [null, [Validators.required, Validators.min(1)]],
      precioUnitarioBS: [null, [Validators.required, Validators.min(1)]],
      totalBS: [null, [Validators.required, Validators.min(1)]]
    });
  }

  onSubmit() {
    if (this.meriendaForm.valid) {
      this.generatePDF();
      this.errorMessage = null;
    } else {
      console.error('Formulario no válido');
      this.errorMessage = 'El formulario no es válido. Por favor, corrija los errores e intente nuevamente.';
    }
  }
  

  private generatePDF() {
    const meriendaData = this.meriendaForm.value;

    const doc = new jsPDF();
    doc.setFontSize(22);
    doc.text('Datos de Merienda', 10, 10);
    doc.setFontSize(12);
    doc.text('Título: ' + meriendaData.titulo, 10, 30);
    doc.text('Detalle: ' + meriendaData.detalle, 10, 40);
    doc.text('Cantidad: ' + meriendaData.cantidad, 10, 50);
    doc.text('Precio Unitario: ' + meriendaData.precioUnitarioBS, 10, 60);
    doc.text('Total: ' + meriendaData.totalBS, 10, 70);

    doc.save('Merienda.pdf');
  }
}
