import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderTipoAComponent } from './header-tipo-a.component';

describe('HeaderTipoAComponent', () => {
  let component: HeaderTipoAComponent;
  let fixture: ComponentFixture<HeaderTipoAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderTipoAComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeaderTipoAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display correct title in h1 tag', () => {
    const h1Element: HTMLElement = fixture.nativeElement.querySelector('h1');
    expect(h1Element.textContent).toContain('Banco Economico S.A.');
  });

  it('should have span elements with class "letter"', () => {
    const spanElements: NodeListOf<HTMLElement> = fixture.nativeElement.querySelectorAll('span.letter');
    expect(spanElements.length).toBeGreaterThan(0);
  });

  it('should have span elements with data-letter attribute', () => {
    const spanElements: NodeListOf<HTMLElement> = fixture.nativeElement.querySelectorAll('span[data-letter]');
    expect(spanElements.length).toBeGreaterThan(0);
  });
});
